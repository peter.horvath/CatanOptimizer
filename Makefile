#!/usr/bin/make -f
c.exe: c.c
	g++ -O6 -Wall -o c c.c
	#g++ -O6 -Wall -S -o - c.c|c++filt

clean:
	rm -vf c c.exe c.s c.S
