#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include <set>

typedef unsigned long long int map;

static const unsigned char typc[8]={'?','W','B','L','Z','E','6','7'};
static const unsigned char numap_typc[8]={'?','2','3','4','5','6','A','B'};
static const int avail[8]={0,4,4,4,3,3,0,0};
static const int numap_avail[8]={0,2,4,4,4,4,0,0};

static const int neigh[18][6]={
	{1,3,4,-1,-1,-1}, // 0
	{0,2,4,5,-1,-1}, // 1
	{1,5,6,-1,-1,-1}, // 2
	{0,4,7,8,-1,-1}, // 3
	{0,1,3,5,8,-1}, // 4
	{1,2,4,6,9,-1}, // 5
	{2,5,9,10,-1,-1}, // 6
	{3,8,11,-1,-1,-1}, // 7
	{3,4,7,11,12,-1}, // 8
	{5,6,10,13,14,-1}, // 9
	{6,9,14,-1,-1,-1}, // 10
	{7,8,12,15,-1,-1}, // 11
	{8,11,13,15,16,-1}, // 12
    {9,12,14,16,17,-1}, // 13
	{9,10,13,17,-1,1}, // 14
	{11,12,16,-1,-1,-1}, // 15
	{12,13,15,17,-1,-1}, // 16
	{13,14,16,-1,-1,-1} // 17
};

static const int perm_rot[18]={
	2, 6, 10,
	1, 5, 9, 14,
	0, 4, 13, 17,
	3, 8, 12, 16,
	7, 11, 13
};

static const int perm_mir[18]={
	2, 1, 0,
	6, 5, 4, 3,
	10, 9, 8, 7,
	14, 13, 12, 11,
	17, 16, 15
};

static const int perm_bw[12][5]={
	{1, 2, 3, 4, 5},
	{1, 2, 3, 5, 4},
	{1, 3, 2, 4, 5},
	{1, 3, 2, 5, 4},
	{2, 1, 3, 4, 5},
	{2, 1, 3, 5, 4},
	{2, 3, 1, 4, 5},
	{2, 3, 1, 5, 4},
	{3, 1, 2, 4, 5},
	{3, 1, 2, 5, 4},
	{3, 2, 1, 4, 5},
	{3, 2, 1, 5, 4}
};

static const int city_neigh[55][4]={
	{ 0, 0, 0, 0}, // 0 (padding only)
	{ 4, 5, 0, 0}, // 1
	{ 5, 6, 0, 0}, // 2
	{ 6, 7, 0, 0}, // 3
	{ 1, 8, 0, 0}, // 4
	{ 1, 2, 9, 0}, // 5
	{ 2, 3,10, 0}, // 6
	{ 3,11, 0, 0}, // 7
	{ 4,12,13, 0}, // 8
	{ 5,13,14, 0}, // 9
	{ 6,14,15, 0}, // 10
	{ 7,15,16, 0}, // 11
	{ 8,17, 0, 0}, // 12
	{ 8, 9,18, 0}, // 13
	{ 9,10,19, 0}, // 14
	{10,11,20, 0}, // 15
	{11,21, 0, 0}, // 16
	{12,22,23, 0}, // 17
	{13,23,24, 0}, // 18
	{14,24,25, 0}, // 19
	{15,25,26, 0}, // 20
	{16,26,27, 0}, // 21
	{17,28, 0, 0}, // 22
	{17,18,29, 0}, // 23
	{18,19,30, 0}, // 24
	{19,20,31, 0}, // 25
	{20,21,32, 0}, // 26
	{21,33, 0, 0}, // 27
	{22,34, 0, 0}, // 28
	{23,34,35, 0}, // 29
	{24,35,36, 0}, // 30
	{25,36,37, 0}, // 31
	{26,37,38, 0}, // 32
	{27,38, 0, 0}, // 33
	{28,29,39, 0}, // 34
	{29,30,40, 0}, // 35
	{30,31,41, 0}, // 36
	{31,32,42, 0}, // 37
	{32,33,43, 0}, // 38
	{34,44, 0, 0}, // 39
	{35,44,45, 0}, // 40
	{36,35,46, 0}, // 41
	{37,46,47, 0}, // 42
	{38,47, 0, 0}, // 43
	{39,40,48, 0}, // 44
	{40,41,49, 0}, // 45
	{41,42,50, 0}, // 46
	{42,43,51, 0}, // 47
	{44,52, 0, 0}, // 48
	{45,52,53, 0}, // 49
	{46,53,54, 0}, // 50
	{47,54, 0, 0}, // 51
	{48,49, 0, 0}, // 52
	{49,50, 0, 0}, // 53
	{50,51, 0, 0}  // 54
};

static const int field_city_neigh[18][6]={
	{1,4,5,8,9,13}, // 0
	{2,5,6,9,10,14}, // 1
	{3,6,7,10,11,15}, // 2
	{8,12,13,17,18,23}, // 3
	{9,13,14,18,19,24}, // 4
	{10,14,15,19,20,25}, // 5
	{11,15,16,20,21,26}, // 6
	{17,22,23,28,29,34}, // 7
	{18,23,24,29,30,35}, // 8
	{20,25,26,31,32,37}, // 9
	{21,26,27,32,33,38}, // 10
	{29,34,35,39,40,44}, // 11
	{30,35,36,44,41,45}, // 12
	{31,36,37,41,42,46}, // 13
	{32,37,38,42,43,47}, // 14
	{40,44,45,48,49,52}, // 15
	{41,45,46,49,50,53}, // 16
	{42,46,47,50,51,54} // 17
};

static int dist[18][18];
static int city_dist[55][55];
static int tried=0;
static int numap_tried=0;
static int found=0;
static int numap_found=0;

static std::set<map> resource_pool;
static std::set<map> numap_pool;

static inline int toc(map m, int n) {
	return typc[ (m>>(n*3))&7 ];
}

static inline int numap_toc(map m, int n) {
	return numap_typc[ (m>>(n*3))&7 ];
}

static inline int at(map m, int pos) {
	return (m>>(3*pos))&7;
}

static inline map to(map m, int pos, int v) {
	//printf ("to: m=%016llx pos=%i v=%i\n", m, pos, v);
	map negmask = ~( ((map)7) << ( (map)(3*pos) ) );
	//printf ("\tnegmask: %016llx\n", negmask);
	map posmask = ((map)v) << (3*pos);
	//printf ("\tposmask: %016llx\n", posmask);
	map result = (m & negmask) | posmask;
	//printf ("\tresult: %016llx\n", result);
	return result;
	//return ( m&~( ((map)7)<<( (map)(3*pos) ) ) ) | ( ((map)v) << (3*pos) );
}

static inline map perm_place(map m, const int* const rel) {
	map r=0;
	for (int a=0;a<18;a++)
		r=to(r,a,at(m,rel[a]));
	return r;
}

static inline map perm_value(map m, const int* const rel) {
	map r=m;
	for (int a=0;a<18;a++)
		r=to(r,a,rel[at(r,a)-1]);
	return r;
}

static void map_print(map m) {
	printf ("    %c   %c   %c   (%016llx) tried=%i found=%i\n"
			"  %c   %c   %c   %c\n"
			"%c   %c       %c   %c\n"
			"  %c   %c   %c   %c\n"
			"    %c   %c   %c\n\n",
			toc(m,  0), toc(m,  1), toc(m,  2), m, tried, found,        toc(m,  3), toc(m, 4), toc(m, 5),
			toc(m,  6), toc(m,  7), toc(m,  8), toc(m,  9), toc(m, 10), toc(m, 11),
			toc(m, 12), toc(m, 13), toc(m, 14), toc(m, 15), toc(m, 16), toc(m, 17));
}

static void numap_print(map m) {
	printf ("    %c   %c   %c   (%016llx) numap_tried=%i numap_found=%i\n"
			"  %c   %c   %c   %c\n"
			"%c   %c       %c   %c\n"
			"  %c   %c   %c   %c\n"
			"    %c   %c   %c\n\n",
			numap_toc(m,  0), numap_toc(m,  1), numap_toc(m,  2), m, numap_tried, numap_found,        numap_toc(m,  3), numap_toc(m,  4), numap_toc(m, 5),
			numap_toc(m,  6), numap_toc(m,  7), numap_toc(m,  8), numap_toc(m,  9), numap_toc(m, 10), numap_toc(m, 11),
			numap_toc(m, 12), numap_toc(m, 13), numap_toc(m, 14), numap_toc(m, 15), numap_toc(m, 16), numap_toc(m, 17));
}

static inline const char R(const map resmap, const int i) {
  return toc(resmap, i);
}

static inline const char N(const map numap, const int i) {
  return numap_toc(numap, i);
}

static void result_print(map r, map n, int resmap_n, int numap_n, float nrg) {
	printf ("    %c%c   %c%c   %c%c   (res: %i/%i, num: %i/%i), nrg: %f\n"
			"  %c%c   %c%c   %c%c   %c%c\n"
			"%c%c   %c%c       %c%c   %c%c\n"
			"  %c%c   %c%c   %c%c   %c%c\n"
			"    %c%c   %c%c   %c%c\n\n",
    R(r, 0), N(n, 0), R(r, 1), N(n, 1), R(r, 2), N(n, 2), resmap_n, found, numap_n, numap_found, nrg,
    R(r, 3), N(n, 3), R(r, 4), N(n, 4), R(r, 5), N(n, 5), R(r, 6), N(n, 6),
    R(r, 7), N(n, 7), R(r, 8), N(n, 8), R(r, 9), N(n, 9), R(r, 10), N(n, 10),
    R(r, 11), N(n, 11), R(r, 12), N(n, 12), R(r, 13), N(n, 13), R(r, 14), N(n, 14),
    R(r, 15), N(n, 15), R(r, 16), N(n, 16), R(r, 17),N(n, 17) );
}

static void dowalk(map m, int d) {
	tried++;

	/* TEST */
	// Neighbors with same resources disallowed
	if (d>0) {
		int a;
		for (a=0;neigh[d-1][a]!=-1;a++) {
			if (at(m, d-1)==at(m, neigh[d-1][a])) {
				//printf ("bad: d=%i a=%i at(m,d)=%c at(m, neigh[d-1][a]=%c\n", d, a, toc(m, d), toc(m, neigh[d-1][a]));
				return;
			}
		}
	}

	// 1) Remove cases if there were too many from a resource
	// 2) Remove redundancy caused by the permutation of the same resources 
	if (d>3) {
		int res[7];
		unsigned int ex=0;
		bzero(&(res[0]),7*sizeof(unsigned int));
		for (int a=0;a<d;a++) {
			if (++res[at(m,a)]>avail[at(m,a)])
				return;
			ex|=(1<<at(m,a));
			if (at(m,a)==2 && !(ex&(1<<1)) )
				return;
			if (at(m,a)==3 && !(ex&(1<<2)) )
				return;
			if (at(m,a)==5 && !(ex&(1<<4)) )
				return;
		}
	}

	/* WALK */
	// If we are not full (deepness < 18), try every child map and return
	if (d<18) {
		int a;
		for (a=1;a<6;a++) {
			dowalk(to(m, d, a), d+1);
		}
		return;
	}

	// tests for the full maps
	//
    // every type of resource field must be reachable in 2 steps from every field
	for (int a=0;a<18;a++) {
		int res[6];
		bzero(&(res[0]),sizeof(int)*6);
		for (int b=0;b<18;b++) {
			if (dist[a][b]<=2)
				res[at(m,b)]++;
		}
		for (int b=1;b<=5;b++) {
			if (res[b]<1)
				return;
		}
	}

	/* permgen */
	//printf ("permutations:\n");
	//
	//Generating alternatives by all symmetries, if one of them already found, return
	for (int mir=0;mir<2;mir++) {
		map m_mir=mir?m:perm_place(m, &(perm_mir[0]));
		map m_rot=m_mir;
		for (int rot=0;rot<6;rot++) {
			for (int vp=0;vp<12;vp++) {
				map m_vp=perm_value(m_rot, &(perm_bw[vp][0]));
				//map_print(m_vp);
				if (resource_pool.find(m_vp)!=resource_pool.end())
					return;
			}
			m_rot=perm_place(m_rot, &(perm_rot[0]));
		}
	}
	//printf ("--permutations.\n");

	/* FOUND */
	found++;
	resource_pool.insert(m);
	
	float qdrs=0;
	for (int a=0;a<18;a++)
		for (int b=0;b<18;b++) {
			if (a==b || at(m,a)!=at(m,b))
				continue;
			qdrs+=1.0/dist[a][b];
		}

	if (qdrs<16.8) {
		map_print(m);
		printf ("qdrs=%f\n",qdrs);
	}
};

static void numap_dowalk(map m, int d) {
	numap_tried++;

	/* TEST */
	// Neighbors with same resources disallowed
	/*
	if (d>0) {
		int a;
		for (a=0;neigh[d-1][a]!=-1;a++) {
			if (at(m, d-1)==at(m, neigh[d-1][a])) {
				return;
			}
		}
	}*/

	// 1) Remove cases if there were too many from a num
	if (d>3) {
		int res[7];
		bzero(&(res[0]),7*sizeof(unsigned int));
		for (int a=0;a<d;a++) {
			if (++res[at(m,a)]>numap_avail[at(m,a)])
				return;
		}
	}

  // Sum worth of the field and its neighbors must be between 10 & 17 for every field
  int sumw;
	for (int a=0;a<d;a++) {
    sumw=at(m,a);
		for (int b=0;neigh[a][b]>-1;b++)
      if (neigh[a][b]<d)
        sumw+=at(m,neigh[a][b]);
      else {
        sumw=-2;
        break;
      }
    if (sumw==-2)
      continue;
    if (sumw<13 || sumw>17)
      return;
	}

	/* WALK */
	// If we are not full (deepness < 18), try every child map and return
	if (d<18) {
		int a;
		for (a=1;a<6;a++)
			numap_dowalk(to(m, d, a), d+1);
		return;
	}

	// tests for the full maps
	/* permgen */
	//printf ("permutations:\n");
	//
	//Generating alternatives by all symmetries, if one of them already found, return
	for (int mir=0;mir<2;mir++) {
		map m_mir=mir?m:perm_place(m, &(perm_mir[0]));
		map m_rot=m_mir;
		for (int rot=0;rot<6;rot++) {
			if (numap_pool.find(m_rot)!=numap_pool.end())
				return;
			m_rot=perm_place(m_rot, &(perm_rot[0]));
		}
	}
	//printf ("--permutations.\n");

	/* FOUND */
	numap_found++;
	numap_pool.insert(m);
};

static float nrg_calc(map res, map num) {
  int prod[55][6];
  float nrg=0;

  // calculating productivity matrix for every city
  for (int c=0;c<=54;c++)
    for (int d=1;d<6;d++)
      prod[c][d]=0;
  for (int c=0;c<18;c++) {
    for (int d=0;d<6;d++) {
      int city=field_city_neigh[c][d];
      prod[city][at(res,c)]+=at(num,c);
    }
  }

  /*
  printf ("prod test dump\n");
  map_print(res); numap_print(num);
  for (int a=1;a<=54;a++) {
    for (int b=1;b<=5;b++)
      printf ("%i ", prod[a][b]);
    printf ("\n");
  }*/

  // calculating energy for every city pair
  for (int c=1;c<=54;c++)
    for (int d=1;d<=54;d++)
      for (int res_typ=1;res_typ<=5;res_typ++) {
        float q1=prod[c][res_typ];
        float q2=prod[d][res_typ];
        float dist=city_dist[c][d]+1;
        nrg+=q1*q2/dist;
      }

  return nrg;
}

static const int eqres_val[18]={2,3,3,4,4,5,5,6,6,8,8,9,9,10,10,11,11,12};
static const int eqres_prob[18]={1,2,2,3,3,4,4,5,5,5,5,4,4,3,3,2,2,1};
static int eqres_tried=0;

static float eqres_minrg=1e+8;

static void eqres_print(map m) {
	for (int a=0;a<18;a++)
		printf ("%i:%c ",eqres_val[a],toc(m,a));
	printf ("\n");
}

static void eqres_dowalk(map m, int d) {
	int res[7];
	eqres_tried++;

	//eqres_print(m);

	/* TESTS */
	// 1) Remove cases if there were too many from a resource
	// 2) Remove redundancy caused by the permutation of the same resources 
	if (d>3) {
		unsigned int ex=0;
		bzero(&(res[0]),7*sizeof(unsigned int));
		for (int a=0;a<d;a++) {
			if (++res[at(m,a)]>avail[at(m,a)])
				return;
			ex|=(1<<at(m,a));
			if (at(m,a)==2 && !(ex&(1<<1)) )
				return;
			if (at(m,a)==3 && !(ex&(1<<2)) )
				return;
			if (at(m,a)==5 && !(ex&(1<<4)) )
				return;
		}
	}

	/* WALK */
	// If we are not full (deepness < 18), try every child map and return
	if (d<18) {
		int a;
		for (a=1;a<6;a++) {
			eqres_dowalk(to(m, d, a), d+1);
		}
		return;
	}

	/* calculating goodness value */
	float nrg=0;
	int res_prod[7];
	bzero(&(res_prod[0]), 7*sizeof(int));
	for (int a=0;a<18;a++)
		res_prod[at(m,a)]+=eqres_prob[a];

	float sum_res_prod=0;
	for (int a=1;a<=5;a++)
		sum_res_prod+=res_prod[a];
	sum_res_prod/=5;
	for (int a=1;a<=5;a++) {
		float dif=sum_res_prod-res_prod[a];
		nrg+=dif*dif;
	}
	if (nrg<eqres_minrg) {
		eqres_minrg=nrg;
		printf ("\nnew optimum found, try: %i, nrg: %f, map: ", eqres_tried, nrg);
		eqres_print(m);
		printf ("res_prod[]={%i,%i,%i,%i,%i} sum_res_prod=%f\n", res_prod[1], res_prod[2], res_prod[3], res_prod[4], res_prod[5], sum_res_prod);
	}
};

static bool prod_test(map res, map num) {
	// every resource must have a productivity 11 or 12. If not, we return
	int res_prod[7];
	bzero(&(res_prod[0]), sizeof(int)*7);
	for (int a=0;a<18;a++)
		res_prod[at(res, a)]+=at(num, a);
	for (int a=1;a<=5;a++)
		if ((res_prod[a]<11)||(res_prod[a]>12))
			return false;
	return true;
}

int main(int argc, char** argv) {
	/*
	printf ("Find best resource-num mapping:\n");
	eqres_dowalk(0, 0);
	*/

	printf ("Calculating field distance matrix:\n");
	for (int a=0;a<18;a++) {
		for (int b=0;b<18;b++)
			dist[a][b]=(a==b)?0:-1;
	}
	bool ex=true;
	for (int ad=1;ex;ad++) {
		ex=false;
		for (int a=0;a<18;a++) {
			for (int b=0;b<18;b++) {
				if (dist[a][b]==ad-1) {
					for (int c=0;neigh[b][c]!=-1;c++) {
						if (dist[a][neigh[b][c]]==-1) {
							dist[a][neigh[b][c]]=ad;
							ex=true;
						}
					}
				}
			}
		}
	}

	for (int a=0;a<18;a++) {
		for (int b=0;b<18;b++)
			printf ("%i, ",dist[a][b]);
		printf("\n");
	}

	printf ("\nGenerating valid resource maps:\n");
	map m=0;
	dowalk(m, 0);

  map *resource_maps=(map*)malloc(sizeof(map)*resource_pool.size());
  int a=0;
  for (std::set<map>::iterator i=resource_pool.begin(); i != resource_pool.end(); i++) {
    resource_maps[a++]=*i;
  }
	printf ("%i valid resource maps found.\n",(int)(resource_pool.size()));

	printf ("\nCalculating city distance matrix:\n");
	for (int a=0;a<=54;a++) {
		for (int b=0;b<=54;b++)
			city_dist[a][b]=(a==b)?0:-1;
	}
	ex=true;
	for (int ad=1;/*(ad<=2)&&*/ex;ad++) {
		ex=false;
		for (int a=0;a<=54;a++) {
			for (int b=0;b<=54;b++) {
				if (city_dist[a][b]==ad-1) {
					for (int c=0;city_neigh[b][c];c++) {
						//printf ("ad=%i a=%i b=%i c=%i\n",ad,a,b,c);
						if (city_dist[a][city_neigh[b][c]]==-1) {
							city_dist[a][city_neigh[b][c]]=ad;
							ex=true;
						}
					}
				}
			}
		}
	}

	printf ("   ");
	for (int a=0;a<=54;a++)
		printf ("%2i ",a);
	printf ("\n");
	for (int a=0;a<=54;a++) {
		printf ("%2i ",a);
		for (int b=0;b<=54;b++)
			printf ("%2i,",city_dist[a][b]);
		printf ("\n");
	}

	printf("\nGenerating valid numaps:\n");
	m=0;
	numap_dowalk(m, 0);

  map *numap_maps=(map*)malloc(sizeof(map)*numap_pool.size());
  a=0;
  for (std::set<map>::iterator i=numap_pool.begin(); i != numap_pool.end(); i++) {
    numap_maps[a++]=*i;
  }
	printf ("%i valid number maps found.\n",(int)(numap_pool.size()));

  /*
  printf ("resmap dump: (%i)\n", found);
  for (int a=0;a<found;a++) {
    printf("%i:\n", a);
    map_print(resource_maps[a]);
  }
  printf ("\nnumap dump:\n");
  for (int a=0;a<numap_found;a++) {
    printf("%i:\n", a);
    numap_print(resource_maps[a]);
  }
  printf ("dumped.\n");
  */

  printf ("Calculating energy for every resmap/numap pair: (%i*%i=%i total)\n", found, numap_found, found*numap_found);
  float minrg=1e+8;
  int n=0;

  unsigned long long int all=numap_found*found;
  unsigned long long int cur=0;
  unsigned long long int done=0;
  unsigned long long int res_id, num_id;
  for (done=0;done<all;done++) {
    res_id=cur%found;
    num_id=cur/found;

    map res=resource_maps[res_id];
    map num=numap_maps[num_id];
    for (int c=0;c<2;c++) {
      map mir=c?num:perm_place(num, &(perm_mir[0]));
      map rot=mir;
      for (int d=0;d<6;d++) {
        if (d)
			rot=perm_place(rot, &(perm_rot[0]));
		if (!prod_test(res, rot))
			continue;

        float nrg=nrg_calc(res, rot);
        n++;
        if (nrg<minrg) {
          printf ("calc %i c=%i d=%i\n", n, c, d);
          result_print(res, rot, res_id, num_id, nrg);
          minrg=nrg;
        }
      }
    }

    cur+=104729;
    cur%=all;
  }

	return 0;
};
